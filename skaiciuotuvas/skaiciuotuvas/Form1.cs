﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skaiciuotuvas
{
    public partial class Form1 : Form
    {
        int a = 0;
        int b = 0;
        int answer = 0;
        string answer2;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            a = Int32.Parse(textBox1.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            answer = a + b;
            label2.Text = "+";
            answer2 = answer.ToString();
            textBox3.Text = answer2;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            answer = a - b;
            label2.Text = "-";
            answer2 = answer.ToString();
            textBox3.Text = answer2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            answer = a * b;
            label2.Text = "X";
            answer2 = answer.ToString();
            textBox3.Text = answer2;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            answer = a / b;
            label2.Text = ":";
            answer2 = answer.ToString();
            textBox3.Text = answer2;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            b = Int32.Parse(textBox2.Text);
        }
    }
}
